package com.example.gamemathtask2

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import kotlin.random.Random

class PlusActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plus)
        var intent = intent

        var countCorrect = 0
        var countIncorrect = 0
        var a  = 0
        var b = 0
        var c  = intent.getIntExtra("Acorrect",0)
        var d = intent.getIntExtra("Aincorrect",0)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            GamePlus(a, b, c, d)
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun GamePlus(a: Int, b: Int, c: Int, d: Int) {
        var num1 = Random.nextInt(1,9)
        var num2 = Random.nextInt(1,9)
        var result = num1+num2
        var txt1: TextView = findViewById(R.id.TV1) as TextView
        txt1.setText("${num1}")
        var txt2: TextView = findViewById(R.id.TV2) as TextView
        txt2.setText("${num2}")
        var btn1: Button = findViewById(R.id.btn_result1) as Button
        var btn2: Button = findViewById(R.id.btn_result2) as Button
        var btn3: Button = findViewById(R.id.btn_result3) as Button
        var btnsave: Button = findViewById(R.id.btn_save) as Button
        var countCorrect = a
        var countIncorrect = b
        var countCorrectText: TextView = findViewById(R.id.countCorrect) as TextView
        var countIncorrectText: TextView = findViewById(R.id.countIncorrect) as TextView
        var txt3 : TextView = findViewById(R.id.TV3) as TextView
        var position = Random.nextInt(1,3)
        if (position==1){
            btn1.setText("${result}")
            btn2.setText("${result+1}")
            btn3.setText("${result+2}")
        }
        if (position==2) {
            btn1.setText("${result-1}")
            btn2.setText("${result}")
            btn3.setText("${result+1}")
        }
        if (position==3) {
            btn1.setText("${result-2}")
            btn2.setText("${result-1}")
            btn3.setText("${result}")
        }
        btn1.setOnClickListener {
            var getTextbtn = btn1.getText()
            if("${result}" == "${getTextbtn}") {
                txt3.visibility = View.VISIBLE
                txt3.setText("Correct")
                txt3.setTextColor(Integer.parseUnsignedInt("ff008000",16))
                countCorrect++
                countCorrectText.setText("${countCorrect}")
                countIncorrectText.setText("${countIncorrect}")
                GamePlus(countCorrect, countIncorrect,c,d)
            }else {
                txt3.visibility = View.VISIBLE
                txt3.setText("Incorrect")
                txt3.setTextColor(Integer.parseUnsignedInt("ffff0000",16))
                countIncorrect++
                countCorrectText.setText("${countCorrect}")
                countIncorrectText.setText("${countIncorrect}")
            }
        }
        btn2.setOnClickListener{
            var getTextbtn = btn2.getText()
            if("${result}" == "${getTextbtn}") {
                txt3.visibility = View.VISIBLE
                txt3.setText("Correct")
                txt3.setTextColor(Integer.parseUnsignedInt("ff008000",16))
                countCorrect++
                countCorrectText.setText("${countCorrect}")
                countIncorrectText.setText("${countIncorrect}")
                GamePlus(countCorrect, countIncorrect,c,d)
            }else {
                txt3.visibility = View.VISIBLE
                txt3.setText("Incorrect")
                txt3.setTextColor(Integer.parseUnsignedInt("ffff0000",16))
                countIncorrect++
                countCorrectText.setText("${countCorrect}")
                countIncorrectText.setText("${countIncorrect}")
            }
        }
        btn3.setOnClickListener{
            var getTextbtn = btn3.getText()
            if("${result}" == "${getTextbtn}") {
                txt3.visibility = View.VISIBLE
                txt3.setText("Correct")
                txt3.setTextColor(Integer.parseUnsignedInt("ff008000",16))
                countCorrect++
                countCorrectText.setText("${countCorrect}")
                countIncorrectText.setText("${countIncorrect}")
                GamePlus(countCorrect, countIncorrect,c,d)
            }else {
                txt3.visibility = View.VISIBLE
                txt3.setText("Incorrect")
                txt3.setTextColor(Integer.parseUnsignedInt("ffff0000",16))
                countIncorrect++
                countCorrectText.setText("${countCorrect}")
                countIncorrectText.setText("${countIncorrect}")
            }
        }
        btnsave.setOnClickListener {
            var totalCorrect= countCorrect + c
            var totalIncorrect = countIncorrect + d
            var intentplus = Intent (this@PlusActivity, MainActivity::class.java)
            intentplus.putExtra("totalCorrect", totalCorrect)
            intentplus.putExtra("totalIncorrect", totalIncorrect)
            startActivity(intentplus)
        }

    }
}