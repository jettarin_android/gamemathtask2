package com.example.gamemathtask2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import org.json.JSONObject.NULL
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var mainCorrect = 0
        var mainIncorrect = 0
        var a = 0
        var b = 0
        var correctText : TextView = findViewById(R.id.countCorrect) as TextView
        var incorrectText : TextView = findViewById(R.id.countIncorrect) as TextView
        correctText.text = "0"
        incorrectText.text = "0"
        var intent = intent
        var totalCorrect = intent.getIntExtra("totalCorrect", a)
        var totalInCorrect = intent.getIntExtra("totalIncorrect",b)
        correctText.text = totalCorrect.toString()
        incorrectText.text = totalInCorrect.toString()
        mainCorrect = a+totalCorrect
        a = totalCorrect
        mainIncorrect = b + totalInCorrect
        b = totalInCorrect



        val btnPlus = findViewById<Button>(R.id.btn_plus)
        btnPlus.setOnClickListener{
            val intent = Intent(MainActivity@this, PlusActivity::class.java)
            intent.putExtra("Acorrect",a)
            intent.putExtra("Aincorrect",b)
            startActivity(intent)
        }
        val btnMinus = findViewById<Button>(R.id.btn_minus)
        btnMinus.setOnClickListener {
            val intent = Intent(MainActivity@this, MinusActivity::class.java)
            intent.putExtra("Acorrect",a)
            intent.putExtra("Aincorrect",b)
            startActivity(intent)
        }
        val btnMultiplied = findViewById<Button>(R.id.btn_multiplied)
        btnMultiplied.setOnClickListener {
            val intent = Intent(MainActivity@this, MultipliedActivity::class.java)
            intent.putExtra("Acorrect",a)
            intent.putExtra("Aincorrect",b)
            startActivity(intent)
        }

    }
}